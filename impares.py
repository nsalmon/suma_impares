def suma_impares_entre_numeros(num1, num2):
    suma = 0
    for num in range(num1, num2 + 1):
        if num % 2 != 0:
            suma += num
    return suma

try:
    num1 = int(input("Dame un número entero no negativo: "))
    if num1 < 0:
        print("El número debe ser no negativo.")
    else:
        num2 = int(input("Dame otro número entero no negativo: "))
        if num2 < 0:
            print("El número debe ser no negativo.")
        else:
            resultado = suma_impares_entre_numeros(min(num1, num2), max(num1, num2))
            print("La suma de los números impares entre", min(num1, num2), "y", max(num1, num2), "es:", resultado)
except ValueError:
    print("Entrada no válida,debes ingresar números enteros no negativos.")

